import { DownloadFileFrontendPage } from './app.po';

describe('download-file-frontend App', () => {
  let page: DownloadFileFrontendPage;

  beforeEach(() => {
    page = new DownloadFileFrontendPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
