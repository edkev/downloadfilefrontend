import { Injectable } from '@angular/core';
import {Http, RequestOptions, ResponseContentType} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

const URL_API_REST = 'http://localhost:8080/rest/';

@Injectable()
export class AppService {

  constructor(private http: Http) { }
  deployement(file: File) {
    console.log('service angular');
    const formData: FormData = new FormData();
    formData.append('file', file, file.name);
    const headers = new Headers();
    headers.append('Content-Type', 'multipart/*');
    // headers.append('Accept', 'application/json');
    this.http.post(URL_API_REST + 'upload', formData, headers)
      .map(res => res.json())
      .catch(error => Observable.throw(error))
      .subscribe(
        data => console.log('success'),
        error => console.log(error)
      )
  }
  downloadfile(filePath: string) {
    return this.http
      .get( URL_API_REST + 'download?filePath=' + filePath, {responseType: ResponseContentType.ArrayBuffer})
      .map(res =>  res)
  }
}
