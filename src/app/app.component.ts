import { Component } from '@angular/core';
import {AppService} from './app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  errorFileExtension: boolean;
  title = 'app';
  filename: string;

  constructor( private appService: AppService) {}

  changeListener($event): void {
    console.log('change listener');
    this.errorFileExtension = false; // to remove the warning each time a change is detected
    this.readThis($event.target);
  }

  readThis(inputValue: any): void {

    const file: File = inputValue.files[0];
    this.filename = file.name;
    const fileExtension: string = this.filename.substr((this.filename.lastIndexOf('.') + 1)); // read the extension of the file

    console.log('test');
    this.appService.deployement(file);
  }


  downloadfile(filePath: string) {
    this.appService.downloadfile(filePath)
      .subscribe(data => this.getZipFile(data))
  }

  private getZipFile(data: any) {
    console.log(JSON.stringify(data));
    const blob = new Blob([data['_body']], { type: 'text/plain' });

    const a: any = document.createElement('a');
    document.body.appendChild(a);

    a.style = 'display: none';
    // var blob = new Blob([data], { type: 'application/zip' });

    const url = window.URL.createObjectURL(blob);

    a.href = url;
    a.download = this.filename;
    a.click();
    window.URL.revokeObjectURL(url);

  }
}
